using Pkg
Pkg.activate(joinpath(@__DIR__, ".."))

using Glob
using HDF5
using GaussianSpin
using Logging
using Printf
using StatsBase
using Turing

# Set to `two_gaussian_model` or `gaussian_spin1_model` as appropriate.
mod = two_gaussian_model

Nsel = 8192
Nsamp = 256
Nmcmc = 1000

sample_seed = 8197203333315979555
selection_seed = 800717895313605042

o3a_dir = "/Users/wfarr/Research/o3a_posterior_samples/all_posterior_samples/"
o3b_dir = "/Users/wfarr/Research/o3b_data/PE/"
selfile = "/Users/wfarr/Research/o3b_data/O3-Sensitivity/endo3_bbhpop-LIGO-T2100113-v12.hdf5"

samps = []
fnames = []
for fn in glob("GW*[0-9].h5", o3a_dir)
    h5open(fn, "r") do f
        push!(samps, read(f, "PublicationSamples/posterior_samples"))
        push!(fnames, fn)
    end
end
for fn in glob("*GW[0-9]*_nocosmo.h5", o3b_dir)
    h5open(fn, "r") do f
        push!(samps, read(f, "C01:Mixed/posterior_samples"))
        push!(fnames, fn)
    end
end

bbh_mask = [quantile([x.mass_2_source for x in s], 0.01) > 3 for s in samps]
bbh_samps = samps[bbh_mask]
bbh_fnames = fnames[bbh_mask]
bbh_XYZ = [samples_to_XYZ(samps) for samps in bbh_samps]
bbh_qs = [[x.mass_ratio for x in s] for s in bbh_samps]

bbh_wts = [map(samps, XYZs) do s, XYZ
    liwts = li_prior_wt(s.mass_1_source, s.mass_ratio, s.redshift, XYZ[1:3], XYZ[4:6])
    popwts = pop_wt(s.mass_1_source, s.mass_ratio, s.redshift, XYZ[1:3], XYZ[4:6])
    popwts / liwts
    end for (samps, XYZs) in zip(bbh_samps, bbh_XYZ)]
bbh_neff = map(wts->round(Int, sum(wts)^2 / sum(wts.*wts)), bbh_wts)
@info "Minimum Neff after re-weighting is $(minimum(bbh_neff))"

bbh_qs_XYZ_rewt = with_seed(sample_seed) do 
    map(bbh_qs, bbh_XYZ, bbh_wts) do bqs, bxs, bws
        inds = sample(1:length(bqs), weights(bws), Nsamp)
        (bqs[inds], bxs[inds])
    end
end
bbh_qs_rewt = [x[1] for x in bbh_qs_XYZ_rewt]
bbh_XYZ_rewt = [x[2] for x in bbh_qs_XYZ_rewt]
bbh_XYZ_wts = [[pop_wt_XYZ(XYZ[1:3], XYZ[4:6]) for XYZ in XYZs] for XYZs in bbh_XYZ_rewt]
bbh_bw = [estimate_bw(s) for s in bbh_XYZ_rewt]

m1sel, qsel, zsel, XYZ1_sel, XYZ2_sel, pdraw, Ndraw = with_seed(selection_seed) do 
    m1sel, qsel, zsel, XYZ1_sel, XYZ2_sel, pdraw, Ndraw = read_selection(selfile)
    @info "Ndraw from selection function resample = $(Ndraw)"
    pdraw = pdraw ./ pop_wt_mqz.(m1sel, qsel, zsel) # Transform to XYZ only.
    inds = sample(1:length(m1sel), Nsel)
    m1sel = m1sel[inds]
    qsel = qsel[inds]
    zsel = zsel[inds]
    XYZ1_sel = XYZ1_sel[inds]
    XYZ2_sel = XYZ2_sel[inds]
    pdraw = pdraw[inds]
    Ndraw = Nsel
    (m1sel, qsel, zsel, XYZ1_sel, XYZ2_sel, pdraw, Ndraw)
end

XYZ_sel = [vcat(x1, x2) for (x1, x2) in zip(XYZ1_sel, XYZ2_sel)]
sel_bw = estimate_bw(XYZ_sel)

model = mod(bbh_qs_rewt, bbh_XYZ_rewt, bbh_bw, bbh_XYZ_wts, qsel, XYZ_sel, sel_bw, pdraw, Ndraw)

if Threads.nthreads() == 1
    trace = sample(model, NUTS(), Nmcmc)
else
    trace = sample(model, NUTS(), MCMCThreads(), Nmcmc, Threads.nthreads())
end
trace = with_logger(NullLogger()) do 
    genq = generated_quantities(model, trace)
    append_generated_quantities(trace, genq)
end

@info @sprintf("Minimum Neff (selection) = %.1f", minimum(trace[:Neff]))
@info @sprintf("Minimum Neff (samples) = %.1f", minimum([minimum(trace[n]) for n in namesingroup(trace, :Neff_samps)]))

if mod == two_gaussian_model
    fname = "two_gaussian.h5"
elseif mod == gaussian_spin1_model
    fname = "gaussian_spin1.h5"
else
    @error "Could not recognize model!"
end

h5open(joinpath(@__DIR__, "..", "chains", fname), "w") do f
    write(f, trace)
end