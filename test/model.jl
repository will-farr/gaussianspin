@testset "model.jl tests" begin
    @testset "positive definite covm" begin
        for _ in 1:100
            sxy = rand()
            sz = rand()
            rho = (2*rand() - 1)/sqrt(2)
            c = covm(sxy, sz, rho)
            @assert all(eigvals(c) .> 0)
        end
    end
end