using GaussianSpin
using LinearAlgebra
using Test

@testset "GaussianSpin.jl Tests" begin
    include("coords.jl")
    include("model.jl")
end