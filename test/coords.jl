@testset "coordinate frame tests" begin
    XYZ = randn(3)
    xyz = to_constrained(XYZ)
    XYZ2 = to_unconstrained(xyz)
    @test all(isapprox.(XYZ, XYZ2))
end