# Fitting Spins as Multivariate Gaussians

Here we explore various ways to fit the spin distribution of the GWTC-3 events with multivariate gaussians in 3D spin space.  (We use an unconstrained coordinate system where S = s / sqrt(1 - s * s), with s the usual spin vector, |s| < 1 and -infinity < S < infinity.)

If you want to get started, you will need to install Julia.  Then start Julia in the top-level directory, and issue 

```julia
> using Pkg
> Pkg.activate(@__DIR__)  # This sets up the environment
> Pkg.instantiate() # This only needs to be run once, downloads all the necessary packages, installs, etc.
> using GaussianSpin
```

Now you're good to go....  If you want, you can install IJulia, and play around with the notebooks in `notebooks` directory.