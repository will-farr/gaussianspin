"""
    to_unconstrained(xyz)

Returns the vector `(X,Y,Z)` in "unconstrained" space corresponding to `(x,y,z)`
(assumed to lie within the unit ball).

The coordinate transformation is 

``X = \\frac{x}{\\sqrt{1 - x^2 - y^2 - z^2}}``

and similarly for `Y` and `Z`.
"""
function to_unconstrained(xyz)
    r2 = dot(xyz, xyz)
    xyz ./ sqrt.(1 .- r2)
end

"""
    to_constrained(XYZ)

The inverse of the `to_unconstrained` transformating, taking `(X,Y,Z)` in R3
back to `(x,y,z)` in the unit ball.
"""
function to_constrained(XYZ)
    r2 = dot(XYZ, XYZ)
    XYZ ./ sqrt.(1 .+ r2)
end

"""
    dxyz_dXYZ(xyz)

Determinant of the coordinate transformation, ``\\left| \\partial(x,y,z) /
\\partial(X,Y,Z) \\right|``.
"""
function dxyz_dXYZ(xyz)
    r2 = dot(xyz, xyz)
    (1 - r2)^(5/2)
end

"""
    samples_to_XYZ(samples)

Converts `samples` containing named tuples of posterior samples to 6D `XYZ` spin
vectors.
"""
function samples_to_XYZ(samples)
    map(samples) do s
        xyz1 = [s.spin_1x, s.spin_1y, s.spin_1z]
        xyz2 = [s.spin_2x, s.spin_2y, s.spin_2z]

        vcat(to_unconstrained(xyz1), to_unconstrained(xyz2))
    end
end

"""
    estimate_bw(XYZ_samples)

Returns a Scott-like estimate for the KDE bandwidth in the 6D space of samples
`XYZ_samples`.
"""
function estimate_bw(XYZ_samples)
    M = hcat(XYZ_samples...)
    cov(M, dims=2) ./ length(XYZ_samples)^(2/(2+6))
end

"""
    tilt(s)

Returns the tilt angle (in radians) for spin vector `s`
"""
function tilt(s)
    acos(s[3] / sqrt(s[1]*s[1] + s[2]*s[2] + s[3]*s[3]))
end