const Z_max = 0.7 # Corresponds to z = -1 if aligned.
const smin = 0.001 # Minimum dispersion.
const q_fit = 0.8 # Where we are fitting the mean S1Z

"""
    covm(sxy, sz, rho)

Constructs the covariance matrix in XYZ space with s.d. in `X` and `Y` `sxy`, in
`Z` of `sz` and correlation coefficient `rho`.
"""
function covm(sxy, sz, rho)
    s2xy = sxy*sxy
    s2z = sz*sz

    [s2xy zero(s2xy) rho*sxy*sz; zero(s2xy) s2xy rho*sxy*sz; rho*sxy*sz rho*sxy*sz s2z]
end

"""
    two_gaussian_model(XYZ_samp, samp_bws, XYZ_sel, sel_bw, N_draw)

Turing model for a Gaussian population for `s_1` and `s_2` in unconstrained
coordinates.

# Arguments

- `q_samp`: Iterable of iterable of floats.  Outer index runs over events; inner
  index runs over posterior samples from each event.

- `XYZ_samp`: Iterable of iterable of 6-vectors.  Outer index runs over events,
  inner index runs over samples from each event of `XYZ` spin coordinates (BH 1
  in 1:3, BH 2 in 4:6). 

- `samp_bw`: Iterable giving the covariance matrix for the KDE bandwidth in (two
  copies of) `XYZ` space to be applied to each event's samples.

- `samp_wts`: Prior density in XYZ space along which the samples have been
  drawn.

- `q_sel`: Samples of `q` from the injections used to determine detector
  sensitivity.

- `XYZ_sel`: Samples of `XYZ` in the spin space of the injections used to
  determine detector sensitivity.

- `sel_bw`: Covariance matrix for KDE bandwidth that will be applied to the
  selection function samples.

- `p_draw`: Density in XYZ space from which the injections have been drawn.

- `N_draw`: The number of overall injections (so that `P_det` is estimated as
  the KDE density over `XYZ_sel` divided by `N_draw`).
"""
@model function two_gaussian_model(q_samp, XYZ_samp, samp_bws, samp_wts, q_sel, XYZ_sel, sel_bw, p_draw, N_draw)
    beta ~ truncated(Normal(2,2), -2, 6)
    mu_Z1 ~ truncated(Normal(0,Z_max/2), -Z_max, Z_max)
    mu_Z2 ~ truncated(Normal(0,Z_max/2), -Z_max, Z_max)

    # Our "fiducial" population is N(0,1), don't want to have heavy tails too likely in the prior...
    sxy1 ~ truncated(Normal(0,Z_max/2), smin, Inf)
    sz1 ~ truncated(Normal(0,Z_max/2), smin, Inf)
    rho_unit1 ~ Beta(3,3) # Slight preference for rho = 0, rules out rho = -1, 1

    sxy2 ~ truncated(Normal(0,Z_max/2), smin, Inf)
    sz2 ~ truncated(Normal(0,Z_max/2), smin, Inf)
    rho_unit2 ~ Beta(3,3)

    # Maximum rho = sqrt(2) because we duplicate the `x` and `y` coordinate shapes.
    rho1 = (2*rho_unit1 - 1) / sqrt(2)
    rho2 = (2*rho_unit2 - 1) / sqrt(2)

    mu_XYZ1 = [zero(mu_Z1), zero(mu_Z1), mu_Z1]
    mu_XYZ2 = [zero(mu_Z2), zero(mu_Z2), mu_Z2]
    mu_XYZ = vcat(mu_XYZ1, mu_XYZ2)

    cov_XYZ1 = covm(sxy1, sz1, rho1)
    cov_XYZ2 = covm(sxy2, sz2, rho2)

    cov_XYZ = cat(cov_XYZ1, cov_XYZ2, dims=(1,2))

    Neff_samps = map(q_samp, XYZ_samp, samp_bws, samp_wts) do qs, samps, samp_bw, wts
        bw = samp_bw .+ cov_XYZ
        d = MvNormal(mu_XYZ, bw)

        ps = [pdf(d, s)/sw*q^beta for (q, s,sw) in zip(qs, samps, wts)]
        mean_ps = mean(ps)
        n = length(ps)

        Turing.@addlogprob!(log(mean_ps))

        n*n*mean_ps*mean_ps / sum(ps.*ps)
    end

    bw = sel_bw .+ cov_XYZ
    d = MvNormal(mu_XYZ, bw)
    ps = [pdf(d, x) / pd * q^beta for (q, x, pd) in zip(q_sel, XYZ_sel, p_draw)]
    mu = sum(ps) / N_draw
    s2 = sum(ps .* ps) / (N_draw*N_draw) - mu*mu/N_draw
    Neff = mu*mu / s2

    Turing.@addlogprob!(-length(XYZ_samp)*log(mu))

    a = q_min^(beta+1)
    b = 1.0
    qbeta = rand(Uniform(min(a,b), max(a,b)))
    q = qbeta^(1/(beta+1))

    d1 = MvNormal(mu_XYZ1, cov_XYZ1)
    XYZ1 = rand(d1)
    xyz1 = to_constrained(XYZ1)
    d2 = MvNormal(mu_XYZ2, cov_XYZ2)
    XYZ2 = rand(d2)
    xyz2 = to_constrained(XYZ2)
    chieff = (xyz1[3] + q*xyz2[3])/(1+q)

    return (mu_XYZ1 = mu_XYZ1, cov_XYZ1 = cov_XYZ1, rho1 = rho1, mu_XYZ2 = mu_XYZ2, cov_XYZ2 = cov_XYZ2, rho2 = rho2, Neff = Neff, Neff_samps = Neff_samps, XYZ1=XYZ1, xyz1=xyz1, XYZ2=XYZ2, xyz2=xyz2, chieff=chieff, q=q)
end

"""
    mu_Z1_relation(mu_Z1, dmu_Z1_dq, q)

Returns the value of the mean `s_{1,Z}` at the given `q` assuming the relation
implied by `mu = mu_Z1` at `q=1` and `mu = mu_Zp1` at `q = q_min` (currently
0.1).
"""
function mu_Z1_relation(mu_Z1, mu_Zp1, q)
  mu_Zp1*(q-1)/(q_min-1) + mu_Z1*(q-q_min)/(1-q_min)
end

function make_mu_XYZ1(Z1z)
  [zero(Z1z), zero(Z1z), Z1z]
end

"""
    gaussian_spin1_model(XYZ_samp, samp_bws, XYZ_sel, sel_bw, N_draw)

Turing model for a Gaussian population (in the unconstrained space) for `s_1`
with `s_2` from a delta function at the origin.  We assume that the Gaussian's
mean `s_{1,z}` depends on `q` and also fit a power-law population in `q`.

# Arguments

- `q_samp`: Iterable of interable of floats.  Outer index runs over events,
  inner index runs over samples from each event's posterior over `q`.

- `XYZ_samp`: Iterable of iterable of 6-vectors.  Outer index runs over events,
  inner index runs over samples from each event of `XYZ` spin coordinates (BH 1
  in 1:3, BH 2 in 4:6). 

- `samp_bw`: Iterable giving the covariance matrix for the KDE bandwidth in (two
  copies of) `XYZ` space to be applied to each event's samples.

- `samp_wts`: Prior density in XYZ space along which the samples have been
  drawn.

- `q_sel`: Samples of `q` of injections used to determine detector sensitivity.

- `XYZ_sel`: Samples of `XYZ` in the spin space of the injections used to
  determine detector sensitivity.

- `sel_bw`: Covariance matrix for KDE bandwidth that will be applied to the
  selection function samples.

- `p_draw`: Density in XYZ space from which the injections have been drawn.

- `N_draw`: The number of overall injections (so that `P_det` is estimated as
  the KDE density over `XYZ_sel` divided by `N_draw`).
"""
@model function gaussian_spin1_model(q_samp, XYZ_samp, samp_bws, samp_wts, q_sel, XYZ_sel, sel_bw, p_draw, N_draw)
    beta ~ truncated(Normal(2,2), -2, 6)
    mu_Z1 ~ truncated(Normal(0,Z_max/2), -Z_max, Z_max)
    mu_Zp1 ~ truncated(Normal(0,Z_max/2), -Z_max, Z_max)

    dmu_Z1_dq = (mu_Z1 - mu_Zp1) / (1 - q_min)
    mu_Z1_fit = mu_Z1_relation(mu_Z1, mu_Zp1, q_fit)

    # Our "fiducial" population is N(0,1), don't want to have heavy tails too likely in the prior...
    sxy1 ~ truncated(Normal(0,Z_max/2), smin, Inf)
    sz1 ~ truncated(Normal(0,Z_max/2), smin, Inf)
    rho_unit1 ~ Beta(3,3) # Slight preference for rho = 0, rules out rho = -1, 1

    # Maximum rho = sqrt(2) because we duplicate the `x` and `y` coordinate shapes.
    rho1 = (2*rho_unit1 - 1) / sqrt(2)

    cov_XYZ1 = covm(sxy1, sz1, rho1)
    cov_XYZ2 = 0 .* cov_XYZ1

    cov_XYZ = cat(cov_XYZ1, cov_XYZ2, dims=(1,2))

    Neff_samps = map(q_samp, XYZ_samp, samp_bws, samp_wts) do qs, samps, samp_bw, wts
        bw = samp_bw .+ cov_XYZ
        
        ps = map(qs, samps, wts) do q, s, w
          mu_XYZ1 = make_mu_XYZ1(mu_Z1_relation(mu_Z1, mu_Zp1, q))
          mu_XYZ2 = zeros(typeof(mu_Z1), 3)
          mu_XYZ = vcat(mu_XYZ1, mu_XYZ2)
          d = MvNormal(mu_XYZ, bw)
          pdf(d, s)/w*q^beta
        end
        mean_ps = mean(ps)
        n = length(ps)

        Turing.@addlogprob!(log(mean_ps))

        n*n*mean_ps*mean_ps / sum(ps.*ps)
    end

    bw = sel_bw .+ cov_XYZ
    ps = map(q_sel, XYZ_sel, p_draw) do q, x, pd
      mu_XYZ1 = make_mu_XYZ1(mu_Z1_relation(mu_Z1, mu_Zp1, q))
      mu_XYZ2 = zeros(typeof(mu_Z1), 3)
      mu_XYZ = vcat(mu_XYZ1, mu_XYZ2)

      d = MvNormal(mu_XYZ, bw)
      pdf(d, x) / pd * q^beta
    end
    mu = sum(ps) / N_draw
    s2 = sum(ps .* ps) / (N_draw*N_draw) - mu*mu/N_draw
    Neff = mu*mu / s2

    Turing.@addlogprob!(-length(XYZ_samp)*log(mu))

    a = 0.1^(beta+1)
    b = 1.0
    qbeta = rand(Uniform(min(a,b), max(a,b)))
    q = qbeta^(1/(beta+1))

    d1 = MvNormal(make_mu_XYZ1(mu_Z1_relation(mu_Z1, mu_Zp1, q)), cov_XYZ1)
    XYZ1 = rand(d1)
    xyz1 = to_constrained(XYZ1)
    XYZ2 = zeros(3)
    xyz2 = to_constrained(XYZ2)
    chieff = (xyz1[3] + q*xyz2[3])/(1+q)

    return (mu_Z1_fit=mu_Z1_fit, dmu_Z1_dq = dmu_Z1_dq, cov_XYZ1 = cov_XYZ1, rho1 = rho1, Neff = Neff, Neff_samps = Neff_samps, XYZ1=XYZ1, xyz1=xyz1, chieff=chieff, q=q)
end