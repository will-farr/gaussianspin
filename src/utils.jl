"""
    cumtrapz(xs, ys)

Return the cumulative trapezoidal estimate of the integral of `ys` over `xs`.

Unlike the `scipy` default, the returned array will be of the same shape as the
inputs; the first element will always be `zero(ys[1])`.
"""
function cumtrapz(xs, ys)
    out = similar(ys)
    out[1] = zero(ys[1])
    for i in 2:length(out)
        out[i] = out[i-1] + 0.5*(xs[i]-xs[i-1])*(ys[i]+ys[i-1])
    end
    out
end

"""
    interp(xs, ys)

Return a function that linearly interpolates a `y(x)` between the given `xs` and
`ys`.

`y(x)` is constant outside the range of the `xs`.  `xs` must be given in
increasing order or the results will be undefined.
"""
function interp(xs, ys)
    function f_interp(x)
        i = searchsortedfirst(xs, x)
        if i == 1
            return ys[1]
        elseif i == length(ys)+1
            return ys[end]
        else
            r = (x - xs[i-1]) / (xs[i]-xs[i-1])
            return r*ys[i] + (1-r)*ys[i-1]
        end
    end

    f_interp
end

"""    with_seed(f, seed)

Perform the zero-argument function `f` after seeding the default RNG with `seed`.

Ensures that the RNG is re-seeded randomly when `f` exits normally or abnormally
(i.e. by throwing an exception).

Note the useful syntax:

    with_seed(1234) do
        ...
    end
"""
function with_seed(f, seed)
    Random.seed!(seed)
    try
        f()
    finally
        Random.seed!()
    end
end

"""
    effective_sample_number(weights)

Returns the effective number of samples from an array of importance weights.
"""
function effective_sample_number(weights)
    s = sum(weights)

    s*s / sum(weights.*weights)
end

"""
    draw_sample_index(wts[, size])

Return an index or indices into the array of `wts` drawn with probability
proportional to `wts`.
"""
function draw_sample_index(wts)
    cwts = cumsum(wts)
    r = rand()*cwts[end]
    searchsortedfirst(cwts, r)
end

function draw_sample_index(wts, size)
    cwts = cumsum(wts)
    [searchsortedfirst(cwts, rand()*cwts[end]) for i in 1:size]
end

import Base: read, write

"""
    write(f::Union{HDF5.File, HDF5.Group}, chains::Chains)

Write MCMCChains object to the HDF5 file or group.
"""
function Base.write(f::Union{HDF5.File, HDF5.Group}, c::Chains)
    for s in sections(c)
        g = create_group(f, string(s))
        for n in names(c, s)
            g[string(n), shuffle=(), compress=3] = Array(c[n])
        end
    end
end

"""
    read(f::Union{HDF5.File, HDF5.Group}, ::Type{Chains})

Read a chain object from the given HDF5 file our group.

"""
function Base.read(f::Union{HDF5.File, HDF5.Group}, ::Type{Chains})
    secs = keys(f)
    pns = []
    datas = []
    name_map = Dict()
    for s in secs
        ns = keys(f[s])
        name_map[Symbol(s)] = ns
        for n in ns
            push!(pns, n)
            push!(datas, read(f[s], n))
        end
    end

    nc, ns = size(datas[1])
    np = size(datas,1)

    a = zeros(nc, np, ns)

    for i in 1:np
        a[:,i,:] = datas[i]
    end

    Chains(a, pns, name_map)
end

# For backwards compatability
function Base.read(::Type{Chains}, f::Union{HDF5.File, HDF5.Group})
    read(f, Chains)
end

function _names(k, d)
    string(k)
end
function _names(k, d::Vector)
    ["$(k)[$(i)]" for i in 1:length(d)]
end
function _names(k, d::Matrix)
    nx, ny = size(d)
    reshape(["$(k)[$(i),$(j)]" for i in 1:nx, j in 1:ny], nx*ny)
end

function _3Dify(d::Array{Float64, 2})
    ns, nc = size(d)
    reshape(d, (ns, 1, nc))
end
function _3Dify(d::Array{Array{Float64, 1}, 2})
    ns, nc = size(d)
    np, = size(d[1,1])

    out = zeros(ns, np, nc)
    for k in 1:nc
        for j in 1:np
            for i in 1:ns
                out[i,j,k] = d[i,k][j]
            end
        end
    end
    out
end
function _3Dify(d::Array{Matrix{Float64}, 2})
    ns, nc = size(d)
    nx, ny = size(d[1,1])

    out = zeros(ns, nx*ny, nc)
    for k in 1:nc
        for l in 1:ny
            for j in 1:nx
                for i in 1:ns
                    out[i, (l-1)*ny + j, k] = d[i,k][j,l]
                end
            end
        end
    end
    out
end

"""
    append_generated_quantities(trace, genq)

Given a trace (`MCMCChains` object) and a 2D array of named tuples corresponding
to `generated_quantities` output, concatenate the two together into a single
`MCMCChains` object.
"""
function append_generated_quantities(trace, genq)
    ks = keys(genq[1,1])
    nms = []
    ds = []
    for k in ks
        d = map(x -> getindex(x, k), genq)
        push!(nms, _names(k, d[1,1]))
        push!(ds, _3Dify(d))
    end
    nms = vcat(nms...)
    ds = cat(ds...; dims=2)

    nmap = trace.name_map
    atrace = cat(Array(trace, keys(nmap), append_chains=false)..., dims=3)
    Chains(cat(atrace, ds, dims=2), vcat(map(string, names(trace)), nms), nmap)
end

"""
    kde_credible_levels(kde, levels)

Returns the contour levels corresponding to the credible levels in `levels`.
"""
function kde_credible_levels(kde, levels)
    den = sort(vec(kde.density))
    cden = cumsum(den)
    clevels = [den[first(searchsorted(cden, l*cden[end]))] for l in levels]
    clevels
end