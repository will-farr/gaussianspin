@userplot Cornerdensity
@recipe function f(cp::Cornerdensity)
    m = cp.args[1]

    nl = get(plotattributes, :levels, 10)
    N = size(m, 1)

    labs = pop!(plotattributes, :label, ["x$i" for i=1:N])
    if labs!=[""] && length(labs)!=N
        error("Number of labels not identical to number of datasets")
    end

    truths = pop!(plotattributes, :truths, nothing)

    legend := false
    layout := (N,N)

    for i in 1:N
        # Do the diagonals
        @series begin
            subplot := i + (i-1)*N
            seriestype := :density
            xlims := (minimum(m[i]), maximum(m[i]))
            ylims := (0, Inf)
            xguide := labs[i]
            x := m[i]
        end

        if truths !== nothing
            @series begin
                subplot := i + (i-1)*N
                seriestype := :vline
                x := [truths[i]]
                seriescolor := :black
                [truths[i]]
            end
        end
    end

    for i in 1:N
        for j in 1:(i-1)
            # Do the kdeplots
            k = kde((m[j], m[i]))
            dv = vec(k.density)
            inds = reverse(sortperm(dv))
            cd = cumsum(dv[inds])
            C = cd[end]

            levels = []
            for i in 1:nl
                f = i/(nl+1)
                cf = f*C
                ind = searchsortedfirst(cd, cf)
                push!(levels, dv[inds[ind]])
            end
            levels = reverse(levels)

            @series begin
                seriestype := :contour
                subplot := (i-1)*N + j
                x := k.x
                y := k.y
                z := permutedims(k.density)
                levels := levels
                xlims := (minimum(m[j]), maximum(m[j]))
                ylims := (minimum(m[i]), maximum(m[i]))
                xguide := labs[j]
                yguide := labs[i]
                k.x, k.y, permutedims(k.density)
            end

            if truths !== nothing
                @series begin
                    seriestype := :vline
                    seriescolor := :black
                    subplot := (i-1)*N + j
                    x := [truths[j]]
                    [truths[j]]
                end

                @series begin
                    seriestype := :hline
                    seriescolor := :black
                    subplot := (i-1)*N + j
                    y := [truths[i]]
                    [truths[i]]
                end
            end
        end
    end

    for i in 1:N
        for j in (i+1):N
            # Do the scatterplots
            @series begin
                seriestype := scatter
                subplot := (i-1)*N + j
                x := m[j]
                y := m[i]
                markersize --> 0.1
                xlims := (minimum(m[j]), maximum(m[j]))
                ylims := (minimum(m[i]), maximum(m[i]))
                xguide := labs[j]
                yguide := labs[i]
                m[j], m[i]
            end

            if truths !== nothing
                @series begin
                    seriestype := :vline
                    seriescolor := :black
                    subplot := (i-1)*N + j
                    x := [truths[j]]
                    [truths[j]]
                end

                @series begin
                    seriestype := :hline
                    seriescolor := :black
                    subplot := (i-1)*N + j
                    y := [truths[i]]
                    [truths[i]]
                end
            end
        end
    end
end

"""
    logscale_ticks(low, high; step=1)

Returns an array of ticks spaced uniformly within each power of ten spanning
from `low` to `high` with `ntick` ticks in each decade.
"""
function logscale_ticks(low, high; step=1)
    xlow = round(Int, floor(log10(low)))
    xhigh = round(Int, ceil(log10(high)))

    xhigh = max(xhigh, xlow+1)

    ticks = []
    for i in xlow:xhigh-1
        x = 10.0^i
        r = 1:9
        for y in x .* r[1:step:end]
            push!(ticks, y)
        end
    end
    push!(ticks, 10.0^xhigh)

    vcat(ticks...)
end

"""
    logscale_ticks(data; step=1)

Automatically determine the span of the ticks from the minimum and maximum
values in `data`.
"""
function logscale_ticks(data; step=1)
    logscale_ticks(minimum(data), maximum(data), step=step)
end
