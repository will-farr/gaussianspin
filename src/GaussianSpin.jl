module GaussianSpin

using Cosmology
using Distributions
using HDF5
using KernelDensity
using LinearAlgebra
using Random
using StatsBase
using StatsPlots
using Turing
using Unitful
using UnitfulAstro

include("coords.jl")
export to_constrained, to_unconstrained
export samples_to_XYZ, dxyz_dXYZ
export estimate_bw
export tilt

include("model.jl")
export covm, gaussian_model, gaussian_spike_model
export two_gaussian_model, two_gaussian_spike_model
export gaussian_spin1_model, q_fit, mu_Z1_relation

include("weights.jl")
export q_min
export li_prior_wt 
export pop_wt, pop_wt_mqz, pop_wt_XYZ
export rewt_selection, read_selection

include("utils.jl")
export cumtrapz, interp, with_seed, effective_sample_number
export draw_sample_index
export append_generated_quantities
export kde_credible_levels

include("plotting.jl")
export cornerdensity, cornerdensity!

end # module
