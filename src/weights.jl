const q_min = 0.1

"""
    li_prior_wt(m1, q, z, XYZ1, XYZ2)

Returns the prior density for LALInference in the input variables.
"""
function li_prior_wt(m1, q, z, XYZ1, XYZ2)
    c = cosmology()
    dL = ustrip(u"Gpc", luminosity_dist(c, z))
    ddLdz = ustrip(u"Gpc", (comoving_transverse_dist(c, z) + (1+z)*hubble_dist(c, 0)/Cosmology.E(c, z)))

    A12 = dot(XYZ1, XYZ1)
    A22 = dot(XYZ2, XYZ2)

    a12 = A12 / (1 + A12)
    a22 = A22 / (1 + A22)

    (1+z)*(1+z)*m1*dL*dL*ddLdz/a12/a22/(1 + A12)^(5/2)/(1+A22)^(5/2)
end

"""
    md_sfr(z)

Returns the Madau-Dickinson star formation rate formula (unnormalized).
"""
function md_sfr(z)
    (1 + z)^2.7 / (1 + ((1+z)/(1+1.9))^5.6)
end

"""
    pop_wt(m1, q, z, XYZ1, XYZ2)

Our fiducial population: flat in `log(m1)`, `q`, and merger rate like the SFR in
the comoving frame.
"""
function pop_wt(m1, q, z, XYZ1, XYZ2)
    wt = pop_wt_mqz(m1, q, z)*pop_wt_XYZ(XYZ1, XYZ2)
    if q < q_min
        zero(wt)
    else
        wt
    end
end

"""
    pop_wt_mqz(m1, q, z)

Returns the fiducial population weight in `m1`, `q`, and `z`.
"""
function pop_wt_mqz(m1, q, z)
    c = cosmology()

    dVdz = ustrip(u"Gpc^3", comoving_volume_element(c, z))

    1/m1*dVdz/(1+z)
end

"""
    pop_wt_XYZ(XYZ1, XYZ2)

The weight applied by our fiducial population to the XYZ spin components for
object 1 and 2.
"""
function pop_wt_XYZ(XYZ1, XYZ2)
    prod(pdf.(Normal(0,0.5), XYZ1))*prod(pdf.(Normal(0,0.5), XYZ2))
end

"""
    rewt_selection(m1s, qs, zs, XYZ1s, XYZ2s, pdraws, Ndraw)

Returns re-sampled `(m1s, qs, zs, XYZ1s, XYZ2s, pdraws, Ndraw)` as if injections
had been drawn from the fiducial population.
"""
function rewt_selection(m1s, qs, zs, XYZ1s, XYZ2s, pdraws, Ndraw)
    pop_wts = pop_wt.(m1s, qs, zs, XYZ1s, XYZ2s)
    wts = pop_wts ./ pdraws
    sum_wts = sum(wts)
    sum_wts2 = sum(wts.*wts)
    norm = sum_wts / Ndraw
    Neff_norm = round(Int, sum_wts*sum_wts / sum_wts2)

    inds = sample(1:length(m1s), weights(wts), Neff_norm)
    norm_pop_wts = pop_wts ./ norm
    m1s[inds], qs[inds], zs[inds], XYZ1s[inds], XYZ2s[inds], norm_pop_wts[inds], Neff_norm
end

function f64ize(x)
    x
end
function f64ize(x::Vector{Float32})
    convert(Vector{Float64}, x)
end

"""
    read_selection(file)

Read the injection set from `file` and return `(m1s, qs, zs, XYZ1s, XYZ2s,
pdraw, Ndraw)` for sensitivity estimation.
"""
function read_selection(file)
    h5open(file, "r") do f
        i = f["injections"]
        m1s = read(i, "mass1_source")
        m2s = read(i, "mass2_source")
        qs = m2s ./ m1s
        zs = read(i, "redshift")
    
        x1 = read(i, "spin1x")
        y1 = read(i, "spin1y")
        z1 = read(i, "spin1z")
    
        x2 = read(i, "spin2x")
        y2 = read(i, "spin2y")
        z2 = read(i, "spin2z")
    
        xyz1 = [[x,y,z] for (x,y,z) in zip(x1,y1,z1)]
        xyz2 = [[x,y,z] for (x,y,z) in zip(x2,y2,z2)]
    
        XYZ1 = [f64ize(to_unconstrained(xyz)) for xyz in xyz1]
        XYZ2 = [f64ize(to_unconstrained(xyz)) for xyz in xyz2]
    
        pm1qz = read(i, "mass1_source_mass2_source_sampling_pdf") .* read(i, "redshift_sampling_pdf") .* m1s
        pXYZ = read(i, "spin1x_spin1y_spin1z_sampling_pdf") .* read(i, "spin2x_spin2y_spin2z_sampling_pdf") .* dxyz_dXYZ.(xyz1) .* dxyz_dXYZ.(xyz2)
        pdraw = pm1qz .* pXYZ

        sel_flag = (read(i, "ifar_cwb") .> 1) .| (read(i, "ifar_gstlal") .> 1) .| (read(i, "ifar_mbta") .> 1) .| (read(i, "ifar_pycbc_bbh") .> 1) .| (read(i, "ifar_pycbc_hyperbank") .> 1)
    
        Ndraw = round(Int, read(attributes(i)["total_generated"]))
    
        map(f64ize, rewt_selection(m1s[sel_flag], qs[sel_flag], zs[sel_flag], XYZ1[sel_flag], XYZ2[sel_flag], pdraw[sel_flag], Ndraw))
    end
end